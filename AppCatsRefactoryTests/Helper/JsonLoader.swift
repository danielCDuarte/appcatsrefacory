//
//  JsonLoader.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

protocol BundleLoader {
    init(bundle: Bundle)
    func load(file name: String, withExtension ext: String) -> Data
}

final class JsonLoader: BundleLoader {
    let bundle: Bundle
    
    init(bundle: Bundle) {
        self.bundle = bundle
    }
    
    func load(file name: String, withExtension ext: String = "json") -> Data {
        guard let filePath = Bundle.main.path(forResource: name, ofType: ext) else {
            fatalError("Invalid resource path with name: \(name) and extension: \(ext)")
        }
        let fileURL = URL(fileURLWithPath: filePath)
        guard let data = try? Data(contentsOf: fileURL) else {
            fatalError("Error on getting data from path \(fileURL.absoluteString)")
        }
        return data
    }
    
}
