//
//  MocksModel.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation
@testable import AppCatsRefactory

class MocksModel {
    
    //MARK: - Properties
    enum FileName: String {
        case breed_cats, breed_cats_detail, my_cats, you_like_cats
    }
    
    //MARK: functions
    static func make<T: Codable>(fileName: FileName) -> T {
        let resultDecoder: ResultDecode = ResultDecode()
        let testBundle: Bundle = Bundle(for: ResultDecode.self)
        let loader: JsonLoader = JsonLoader(bundle: testBundle)
        let data: Data = loader.load(file: fileName.rawValue)
        let result: Result<T, Error> = resultDecoder.decode(data: data)
        
        switch result {
        case .success(let model):
            return model
        default:
            fatalError("Should not be nil")
        }
    }
    
    static func makeData<T>(fileName: FileName) -> T {
        let testBundle: Bundle = Bundle(for: ResultDecode.self)
        let loader: JsonLoader = JsonLoader(bundle: testBundle)
        let data: Data = loader.load(file: fileName.rawValue)
        return data as! T
    }
}
