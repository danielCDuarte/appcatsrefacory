//
//  BreedCatsDetailViewSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import AppCatsRefactory

class BreedCatsDetailViewSpec: QuickSpec {
    override func spec() {
        describe("BreedCatsDetailViewSpec") {
            
            var sut: BreedCatsDetailView!
            var mainVC: UIViewController!
            let delegate = BreedCatsDetailViewDelegateMock()
            var breedDetail: BreedDetailModel?
                
            beforeEach {
              
                breedDetail = MocksModel.make(fileName: .breed_cats_detail)
                sut = BreedCatsDetailView(delegate: delegate)
                sut.setupView(breedDetail: breedDetail)
                
                mainVC = UIViewController()
                mainVC.view = sut
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated.", {
                it("Should be a valid snapshot.", closure: {
                    expect(UIWindow.testWindow) == snapshot("BreedCatsDetailViewSpec")
              
                })
                it("Should be a called didOpenWiki action.", closure: {
                    expect(delegate.validateCalledDidOpenWiki).to(beFalse())
                    sut.linkbutton.sendActions(for: .touchUpInside)
                    expect(delegate.validateCalledDidOpenWiki).to(beTrue())
                
                })
                
                it("Should be a init With coder.", closure: {
                    sut = BreedCatsDetailView(coder: NSCoder())
                    expect(sut).to(beNil())
                })
            })
        }
    }
}

