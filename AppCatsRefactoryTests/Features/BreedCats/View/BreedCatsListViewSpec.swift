//
//  BreedCatsListViewSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import AppCatsRefactory

class BreedCatsListViewSpec: QuickSpec {
    override func spec() {
        describe("BreedCatsListViewSpec") {
            var sut: BreedCatsListView!
            var mainVC: UIViewController!
            let delegate = BreedCatsListViewDelegateMock()
            var breeds: [BreedModel]?
            
            beforeEach {
                
                breeds = MocksModel.make(fileName: .breed_cats)
                sut = BreedCatsListView(pagenatorIndex: 0, delegate: delegate)
                sut.setup(breedList: breeds!)
                
                mainVC = UIViewController()
                mainVC.view = sut
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View is instanciated.", {
                it("Should be a valid snapshot.", closure: {
                    expect(UIWindow.testWindow) == snapshot("BreedCatsListViewSpec")
                
                })
                it("Should be a called didSelect action.", closure: {
                    let indexPath = IndexPath(row: 0, section: 0)
                    expect(delegate.validateCalledDidSelect).to(beFalse())
                    sut.tableView(sut.tableView, didSelectRowAt: indexPath)
                    expect(delegate.validateCalledDidSelect).to(beTrue())
                
                })
                it("Should be a init With coder.", closure: {
                    sut = BreedCatsListView(coder: NSCoder())
                    expect(sut).to(beNil())
                })
                
            })
            
        }
        
    }
    
}
