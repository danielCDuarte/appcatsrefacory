//
//  BreedCatsListViewDelegateMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/6/20.
//

import Foundation

@testable import AppCatsRefactory

class BreedCatsListViewDelegateMock: BreedCatsListViewDelegate {
    
    //MARK: - Properties
    var validateCalledDidSelect: Bool = false
    var breed: BreedModel?
    
    var validateCalledDidPaginator: Bool = false
    var page: Int?
   
    //MARK: - Functions
    
    
    func didSelect(breed: BreedModel) {
        validateCalledDidSelect = true
        self.breed = breed
    }
    
    func didPaginator(page: Int) {
        validateCalledDidPaginator = true
        self.page = page
    }
}
