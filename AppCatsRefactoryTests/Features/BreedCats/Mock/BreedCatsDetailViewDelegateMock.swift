//
//  BreedCatsDetailViewDelegateMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/6/20.
//

import Foundation

@testable import AppCatsRefactory

class BreedCatsDetailViewDelegateMock: BreedCatsDetailViewDelegate {
    
    //MARK: - Properties
    var validateCalledDidOpenWiki: Bool = false
    var url: URL?
    
    //MARK: functions
    func didOpenWiki(url: URL) {
        validateCalledDidOpenWiki = true
        self.url = url
    }
}
