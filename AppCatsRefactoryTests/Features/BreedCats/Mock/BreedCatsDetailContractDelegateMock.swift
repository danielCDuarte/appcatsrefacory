//
//  BreedCatsDetailContractDelegateMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Foundation

@testable import AppCatsRefactory

class BreedCatsDetailContractDelegateMock: BreedCatsDetailContractDelegate {
    //MARK: - Properties
    var validateCalledStartLoading: Bool = false
    
    var validateCalledFinishLoading: Bool = false
    
    
    //MARK: - Functions
    func startLoading() {
        validateCalledStartLoading = true
    }
    
    func finishLoading() {
        validateCalledFinishLoading = true
    }
    
}
