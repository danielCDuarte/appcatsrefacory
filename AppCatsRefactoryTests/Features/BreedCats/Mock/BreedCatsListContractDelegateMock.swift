//
//  BreedCatsListContractDelegateMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Foundation

@testable import AppCatsRefactory

class BreedCatsListContractDelegateMock: BreedCatsListContractDelegate {
    
    //MARK: - Properties
    var validateCalledStartLoading: Bool = false
    
    var validateCalledFinishLoading: Bool = false
    
    var validateSetBreedCats: Bool = false
    var validateBreedCats: [BreedModel]?
    
    var validateSetEmptyBreedCats: Bool = false
    var validateMessageSetEmptyBreedCats: String?
    
    var validateSetDetailBreed: Bool = false
    var validateDetailBreed: BreedDetailModel?
    
    var validateSetEmptyDetailBreed: Bool = false
    var validateMessageSetEmptyDetailBreed: String?
    
    //MARK: - Functions
    func startLoading() {
        validateCalledStartLoading = true
    }
    
    func finishLoading() {
        validateCalledFinishLoading = true
    }
    
    func setBreedCats(breedCats: [BreedModel]) {
        validateSetBreedCats = true
        validateBreedCats = breedCats
    }
    
    
    func setEmptyBreedCats(message: String) {
        validateSetEmptyBreedCats = true
        validateMessageSetEmptyBreedCats = message
    }
    
    func setDetailBreed(detailBreed: BreedDetailModel) {
        validateSetDetailBreed = true
        validateDetailBreed = detailBreed
    }
    
    func setEmptyDetailBreed(message: String) {
        validateSetEmptyDetailBreed = true
        validateMessageSetEmptyDetailBreed = message
    }
    

}
