//
//  BreedCatsDetailViewControllerSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Quick
import Nimble
import Nimble_Snapshots
@testable import AppCatsRefactory

class BreedCatsDetailViewControllerSpec: QuickSpec {
    override func spec() {
        describe("BreedCatsDetailViewControllerSpec") {
            var sut: BreedCatsDetailViewController!
            let contractDelegate: BreedCatsDetailContractDelegateMock = BreedCatsDetailContractDelegateMock()
            let breedDetailExpect: BreedDetailModel = MocksModel.make(fileName: .breed_cats_detail)
            
            beforeEach {
                let breedCatsDetailPresenter: BreedCatsDetailPresenter = BreedCatsDetailPresenter()
                breedCatsDetailPresenter.attachView(view: contractDelegate)
                sut = BreedCatsDetailViewController(breedCatsDetailPresenter: breedCatsDetailPresenter, breedDetail: breedDetailExpect)
                _ = sut.view
                UIWindow.setTestWindow(rootViewController: sut)
            }
            afterEach {
                UIWindow.cleanTestWindow()
                sut = nil
            }
            
            context("When View controller is instanciated", {
               
                it("should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("BreedCatsDetailViewControllerSpec")
                })
                it("Should be a init With coder.", closure: {
                    sut = BreedCatsDetailViewController(coder: NSCoder())
                    expect(sut).to(beNil())
                })
            })
        }
    }
}
