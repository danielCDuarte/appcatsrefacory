//
//  BreedCatsListViewControllerSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Quick
import Nimble
import Nimble_Snapshots
@testable import AppCatsRefactory

class BreedCatsListViewControllerSpec: QuickSpec {
    override func spec() {
        describe("BreedCatsListViewControllerSpec") {
            var sut: BreedCatsListViewController!
            let sessionMock: URLSessionMock = URLSessionMock()
            let resultDecodeMock: ResultDecodeMock = ResultDecodeMock()
            let networkSessionMock: NetworkSessionMock = NetworkSessionMock(session: sessionMock)
            let contractDelegate = BreedCatsListContractDelegateMock()
            
            let dataExpect: Data = MocksModel.makeData(fileName: .breed_cats)
            let breedsExpect: [BreedModel] = MocksModel.make(fileName: .breed_cats)
            let errorExpect: Error = APIClientError.noData
            
            sessionMock.data = dataExpect
            sessionMock.error = errorExpect
            resultDecodeMock.data = breedsExpect
            resultDecodeMock.error = errorExpect
            networkSessionMock.data = dataExpect
            networkSessionMock.error = errorExpect
            
            beforeEach {
                
                let apiClient: ApiClient = ApiClient(networkSession: networkSessionMock, resultDecoder: resultDecodeMock)
                var breedCatsListPresenter: BreedCatsListPresenter = BreedCatsListPresenter(apiClient: apiClient)
                breedCatsListPresenter.attachView(view: contractDelegate)
                sut = BreedCatsListViewController(breedCatsListPresenter: breedCatsListPresenter)
                _ = sut.view
                UIWindow.setTestWindow(rootViewController: sut)
            }
            afterEach {
                UIWindow.cleanTestWindow()
                sut = nil
            }
            
            context("When View controller is instanciated", {
                
                it("should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("BreedCatsListViewControllerSpec")
                })
                it("Should be a init With coder.", closure: {
                    sut = BreedCatsListViewController(coder: NSCoder())
                    expect(sut).to(beNil())
                })
            })
        }
    }
}
