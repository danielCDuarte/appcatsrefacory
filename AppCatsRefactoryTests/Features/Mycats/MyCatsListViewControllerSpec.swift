//
//  MyCatsListViewControllerSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Quick
import Nimble
import Nimble_Snapshots
import RealmSwift
@testable import AppCatsRefactory

class MyCatsListViewControllerSpec: QuickSpec {
    override func spec() {
        describe("MyCatsListViewControllerSpec") {
            var sut: MyCatsListViewController!
            let contractDelegate: MyCatsListContractDelegateMock = MyCatsListContractDelegateMock()
            let realm: Realm!
            
            beforeSuite {
                  Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
            }
            
            beforeEach {
                let myCatsListPresenter: MyCatsListPresenter = MyCatsListPresenter(realm: try! Realm())
                myCatsListPresenter.attachView(view: contractDelegate)
                sut = MyCatsListViewController(myCatsListPresenter: myCatsListPresenter)
                _ = sut.view
                UIWindow.setTestWindow(rootViewController: sut)
            }
            afterEach {
                UIWindow.cleanTestWindow()
                sut = nil
            }
            
            context("When View controller is instanciated", {
                it("should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("MyCatsListViewControllerSpec")
                })
                it("Should be a init With coder.", closure: {
                    sut = MyCatsListViewController(coder: NSCoder())
                    expect(sut).to(beNil())
                })
            })
        }
    }
}
