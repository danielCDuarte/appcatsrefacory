//
//  MyCatsListContractDelegateMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Foundation

@testable import AppCatsRefactory

class MyCatsListContractDelegateMock: MyCatsListContractDelegate {
    //MARK: - Properties
    var validateCalledStartLoading: Bool = false
    
    var validateCalledFinishLoading: Bool = false
    
    var validateCalledSetMyCats: Bool = false
    var validateMyCats: [LikeCatModel]?
    
    var validateCalledSetEmptyMyCats: Bool = false
    var validateMessage: String?
    
    //MARK: - Functions
    func startLoading() {
        validateCalledStartLoading =  true
    }
    
    func finishLoading() {
        validateCalledFinishLoading = true
    }
    
    func setMyCats(myCats: [LikeCatModel]) {
        validateCalledSetMyCats = true
        validateMyCats = myCats
    }
    
    func setEmptyMyCats(message: String) {
        validateCalledSetEmptyMyCats = true
        validateMessage = message
    }
    
}
