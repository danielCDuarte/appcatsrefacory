//
//  BlurLoaderSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/6/20.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import AppCatsRefactory

class BlurLoaderSpec: QuickSpec {
    override func spec() {
        describe("BlurLoaderSpec") {
            
            var sut: UIView!
            var mainVC: UIViewController!
            beforeEach {
             
                sut = UIView(frame: .zero)
                sut.backgroundColor = .white
                mainVC = UIViewController()
                mainVC.view = sut
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated.", {
                it("Should be a show BlurLoader valid snapshot.", closure: {
                    sut.showBlurLoader()
                    expect(UIWindow.testWindow) == snapshot("ShowBlurLoaderSpec")
                })
                it("Should be a remove BlurLoader valid snapshot.", closure: {
                    sut.removeBluerLoader()
                    expect(UIWindow.testWindow) == snapshot("RemoveBlurLoaderSpec")
                })
            })
        }
    }
}

