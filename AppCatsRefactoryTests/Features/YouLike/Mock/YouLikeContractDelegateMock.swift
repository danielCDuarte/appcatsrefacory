//
//  YouLikeContractDelegateMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Foundation

@testable import AppCatsRefactory

class YouLikeContractDelegateMock: YouLikeContractDelegate {
    
    //MARK: - Properties
    var validateCalledStartLoading: Bool = false
    
    var validateCalledFinishLoading: Bool = false
    
    var validateCalledSetFavorites: Bool = false
    var validateYouLikes: [YouLikeModel]?
    
    var validateSetEmptyFavorites: Bool = false
    var validateTitleEmptyFavorites: String?
    var validateMessageEmptyFavorites: String?
    
    var validateSetSaveFavorite: Bool = false
    
    var validateSetErrorSaveFavorite: Bool = false
    var validateMessageErrorSaveFavorite: String?
    
    func startLoading() {
        validateCalledStartLoading = true
    }
    
    func finishLoading() {
        validateCalledFinishLoading = true
    }
    
    func setFavorites(YouLikes: [YouLikeModel]) {
        validateCalledSetFavorites = true
        validateYouLikes = YouLikes
    }
    
    func setEmptyFavorites(title: String, message: String) {
        validateSetEmptyFavorites = true
        validateTitleEmptyFavorites = title
        validateMessageEmptyFavorites = message
        
    }
    
    func setSaveFavorite() {
        validateSetSaveFavorite = true
    }
    
    func setErrorSaveFavorite(message: String) {
        validateSetErrorSaveFavorite = true
        validateMessageErrorSaveFavorite = message
    }
    
}
