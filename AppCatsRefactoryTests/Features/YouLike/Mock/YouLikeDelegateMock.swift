//
//  YouLikeDelegateMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/6/20.
//

import Foundation

@testable import AppCatsRefactory

class YouLikeDelegateMock: YouLikeDelegate {
    
    //MARK: properties
    var validatedidItsLike: Bool = false
    var youLike: YouLikeModel?
    
    //MARK: functions
    func didItsLike(youLike: YouLikeModel) {
        validatedidItsLike = true
        self.youLike = youLike
    }
    
}
