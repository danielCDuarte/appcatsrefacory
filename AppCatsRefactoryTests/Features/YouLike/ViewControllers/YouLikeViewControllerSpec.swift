//
//  YouLikeViewControllerSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Quick
import Nimble
import Nimble_Snapshots
import RealmSwift
@testable import AppCatsRefactory

class YouLikeViewControllerSpec: QuickSpec {
    override func spec() {
        describe("YouLikeViewControllerSpec") {
            var sut: YouLikeViewController!
            let sessionMock: URLSessionMock = URLSessionMock()
            let resultDecodeMock: ResultDecodeMock = ResultDecodeMock()
            let networkSessionMock: NetworkSessionMock = NetworkSessionMock(session: sessionMock)
            let contractDelegate: YouLikeContractDelegateMock = YouLikeContractDelegateMock()
            
            let dataExpect: Data = MocksModel.makeData(fileName: .you_like_cats)
            let breedsExpect: [YouLikeModel] = MocksModel.make(fileName: .you_like_cats)
            let errorExpect: Error = APIClientError.noData
            
            let youLikesExpect: [YouLikeModel] = MocksModel.make(fileName: .you_like_cats)
            
            sessionMock.data = dataExpect
            sessionMock.error = errorExpect
            resultDecodeMock.data = breedsExpect
            resultDecodeMock.error = errorExpect
            networkSessionMock.data = dataExpect
            networkSessionMock.error = errorExpect
            
            let realm: Realm!
            
            beforeSuite {
                  Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
            }
            
            beforeEach {
                let apiClient: ApiClient = ApiClient(networkSession: networkSessionMock, resultDecoder: resultDecodeMock)
                let youLikePresenter: YouLikePresenter = YouLikePresenter(apiClient: apiClient, realm: try! Realm())
                youLikePresenter.attachView(view: contractDelegate)
                sut = YouLikeViewController(youLikePresenter: youLikePresenter)
                _ = sut.view
                UIWindow.setTestWindow(rootViewController: sut)
            }
            afterEach {
                UIWindow.cleanTestWindow()
                sut = nil
            }
            
            context("When View controller is instanciated", {
                it("should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("YouLikeViewControllerSpec")
                })
                it("Should be a init With coder.", closure: {
                    sut = YouLikeViewController(coder: NSCoder())
                    expect(sut).to(beNil())
                })
            })
        }
    }
}
