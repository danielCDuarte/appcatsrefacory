//
//  YouLikeViewSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import AppCatsRefactory

class YouLikeViewSpec: QuickSpec {
    override func spec() {
        describe("YouLikeViewSpec") {
            
            var sut: YouLikeView!
            var mainVC: UIViewController!
            var youLikes: [YouLikeModel]?
            let delegate: YouLikeDelegateMock = YouLikeDelegateMock()
            beforeEach {
                youLikes = MocksModel.make(fileName: .you_like_cats)
                sut = YouLikeView(delegate: delegate)
                sut.setup(youLikeModel: (youLikes?.first)!)
                
                mainVC = UIViewController()
                mainVC.view = sut
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated.", {
                it("Should be a valid snapshot.", closure: {
                    expect(UIWindow.testWindow) == snapshot("YouLikeViewSpec")
              
                })
                it("Should be a called like, didItsLike action.", closure: {
                    expect(delegate.validatedidItsLike).to(beFalse())
                    sut.likeButton.sendActions(for: .touchUpInside)
                    expect(delegate.validatedidItsLike).to(beTrue())
                
                })
                it("Should be a called unlike, didItsLike action.", closure: {
                    delegate.validatedidItsLike = false
                    sut.disLikeButton.sendActions(for: .touchUpInside)
                    expect(delegate.validatedidItsLike).to(beTrue())
                
                })
                
                it("Should be a init With coder.", closure: {
                    sut = YouLikeView(coder: NSCoder())
                    expect(sut).to(beNil())
                })
            })
        }
    }
}
