//
//  TabBarAppControllerSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/6/20.
//

import UIKit
import Quick
import Nimble
import Nimble_Snapshots
import RealmSwift

@testable import AppCatsRefactory

class TabBarAppControllerSpec: QuickSpec {

    override func spec() {
        describe("TabBarAppControllerSpec") {
            var sut: TabBarAppController!
            beforeEach {

                let oneViewController: UIViewController = UIViewController()
                oneViewController.addTabBarIcon(imageNamed: "footPrintTab")
                
                let twoViewController: UIViewController = UIViewController()
                twoViewController.addTabBarIcon(imageNamed: "heartHalfTab")
                
                let threeViewController: UIViewController = UIViewController()
                threeViewController.addTabBarIcon(imageNamed: "catOnlineTab")
                
                sut = TabBarAppController()
                
                sut.viewControllers = [
                    oneViewController,
                    twoViewController,
                    threeViewController
                ]
                
                UIWindow.setTestWindow(rootViewController: sut)
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            context("when TabBarAppController is instanciated.", {
                it("should be a valid snapshot.", closure: {
                    expect(UIWindow.testWindow) == snapshot("TabBarAppControllerSpec")
                })
                it("Should be a init With coder.", closure: {
                    sut = TabBarAppController(coder: NSCoder())
                    expect(sut).to(beNil())
                })
            })
        }
    }
}
