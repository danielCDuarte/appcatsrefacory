//
//  TabBarAppController.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/6/20.
//

import Foundation
import RealmSwift
import UIKit

class TabBarAppController: UITabBarController, UITabBarControllerDelegate {
    
    // MARK: - init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.tabBar.configure()
    }
     
}
