//
//  ApiClientSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/6/20.
//

import Foundation
import Quick
import Nimble
@testable import AppCatsRefactory

class ApiClientSpec: QuickSpec {
    override func spec(){
        describe("ApiClientSpec"){
            var sut: ApiClient!
            let sessionMock = URLSessionMock()
            let resultDecode = ResultDecodeMock()
            let networkSessionMock: NetworkSessionMock = NetworkSessionMock(session: sessionMock)
            
            let dataExpect: Data = MocksModel.makeData(fileName: .breed_cats)
            let breedsExpect: [BreedModel] = MocksModel.make(fileName: .breed_cats)
            let errorExpect: Error = APIClientError.noData
            
            sessionMock.data = dataExpect
            sessionMock.error = errorExpect
            resultDecode.data = breedsExpect
            resultDecode.error = errorExpect
            networkSessionMock.data = dataExpect
            networkSessionMock.error = errorExpect
           
            let requestParamsByGet: RequestParams = RequestParams(
                service: .allBreeds,
                method: .get,
                headers: ["A": "a", "B": "b"],
                params: ["limit": String(20), "page": String(1)]
            )
            
            let requestParamsBYPost: RequestParams = RequestParams(
                service: .allBreeds,
                method: .post,
                headers: ["A": "a", "B": "b"],
                params: ["limit": String(20), "page": String(1)]
            )
            
            beforeEach {
                sut = ApiClient(networkSession: networkSessionMock, resultDecoder: resultDecode)
                
            }
            
            afterEach {
                sut = nil
            }
            
            context("When ApiClient was called by type get.") {
                
                it("Should be a called loadRequest success.", closure: {
                    networkSessionMock.resultExpected = .success
                    resultDecode.resultExpected = .success
                    sut.loadRequest(requestParams: requestParamsByGet) { (result: Result<[BreedModel],Error>) in
                        switch result{
                        case .success(let data):
                            expect(breedsExpect.first?.id) == data.first?.id
                        case .failure( _): break
                        }

                    }
                })
                
                it("Should be a called loadRequest failure.", closure: {
                    networkSessionMock.resultExpected = .failure
                    resultDecode.resultExpected = .failure
                    sut.loadRequest(requestParams: requestParamsByGet) { (result: Result<[BreedModel],Error>) in
                        switch result{
                        case .success( _): break
                        case .failure(let error):
                            expect(errorExpect.localizedDescription) == error.localizedDescription
                        }
                        
                    }
                })
            }
            
            context("When ApiClient was called by type post.") {
                
                it("Should be a called loadRequest success.", closure: {
                    networkSessionMock.resultExpected = .success
                    resultDecode.resultExpected = .success
                    sut.loadRequest(requestParams: requestParamsBYPost) { (result: Result<[BreedModel],Error>) in
                        switch result{
                        case .success(let data):
                            expect(breedsExpect.first?.id) == data.first?.id
                        case .failure( _): break
                        }
                    }
                })
                
                it("Should be a called loadRequest failure.", closure: {
                    networkSessionMock.resultExpected = .failure
                    resultDecode.resultExpected = .failure
                    sut.loadRequest(requestParams: requestParamsBYPost) { (result: Result<[BreedModel],Error>) in
                        switch result{
                        case .success( _): break
                        case .failure(let error):
                            expect(errorExpect.localizedDescription) == error.localizedDescription
                        }
                    }
                })
            }
            
        }
    }
}
