//
//  ResultDecoderSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation
import Quick
import Nimble
@testable import AppCatsRefactory

class ResultDecoderSpec: QuickSpec {
    override func spec(){
        describe("ResultDecoderSpec"){
            var sut: ResultDecodeMock!
            let breedsExpect: [BreedModel] = MocksModel.make(fileName: .breed_cats)
            let dataExpect: Data = MocksModel.makeData(fileName: .breed_cats)
            let errorExpect: Error = APIClientError.noData
            beforeEach {
                sut = ResultDecodeMock()
                sut.data = breedsExpect
                sut.error = APIClientError.noData
            }
            
            afterEach {
                sut = nil
            }
            
            context("When ResultDecode was called.") {
                
                it("Should be a called decode success.", closure: {
                    sut.resultExpected = .success
                    let resultDecode: Result<Codable, Error> = sut.decode(data: dataExpect)
                    switch resultDecode{
                    case .success(let data):
                        let dataValidate: [BreedModel] = data as! [BreedModel]
                        expect(breedsExpect.first?.id) == dataValidate.first?.id
                        print(data)
                    case .failure( _): break
                    }
                })
                
                it("Should be a called decode failure.", closure: {
                    sut.resultExpected = .failure
                    let resultDecode: Result<Codable, Error> = sut.decode(data: dataExpect)
                    switch resultDecode{
                    case .success( _): break
                    case .failure(let error):
                        expect(errorExpect.localizedDescription) == error.localizedDescription
                    }
                    
                })
                
            }
        }
    }
    
}
