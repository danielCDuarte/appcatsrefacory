//
//  NetworkSessionSpec.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation
import Quick
import Nimble
@testable import AppCatsRefactory

class NetworkSessionSpec: QuickSpec {
    override func spec(){
        describe("NetworkSessionSpec"){
            var sut: NetworkSessionMock!
            let sessionMock: URLSessionMock = URLSessionMock()
            let urlRquest = URLRequest(Resource(url: URL(string: "https://www.google.com/")!, method: .get))
            let dataSuccess: Data = MocksModel.makeData(fileName: .breed_cats)
            let errorFailure: Error = APIClientError.noData
            beforeEach {
                sut = NetworkSessionMock(session: sessionMock)
                sut.data = dataSuccess
                sut.error = errorFailure
                
            }
            afterEach {
                sut.resultExpected = .failure
            }

            context("When NetworkSession was called.") {
                it("Should be a called didLoad success.") {
                    sut.resultExpected = .success
                    sut.didLoad(urlRquest) { (result: Result<Data,Error>) in
                        switch result {
                        case .success(let data):
                            expect(dataSuccess) == data
                        case .failure( _): break
                        }
                    }
                }
                it("Should be a called didLoad failure.") {
                    sut.resultExpected = .failure
                    sut.didLoad(urlRquest) { (result: Result<Data,Error>) in
                        switch result {
                        case .success( _): break
                        case .failure(let error):
                            expect(errorFailure.localizedDescription) == error.localizedDescription
                        }
                    }
                }
            }
        }
    }
}
