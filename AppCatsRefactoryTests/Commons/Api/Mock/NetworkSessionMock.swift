//
//  NetworkSessionMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/7/20.
//

import Foundation

@testable import AppCatsRefactory

class NetworkSessionMock: NetworkSession {
    
    //MARK: - Properties
    var resultExpected: ResultExpectedEnum = .failure
    var data: Data?
    var error: Error?
    
    
    //MARK: - Functions
    override func didLoad(_ request: URLRequest, result: @escaping ((Result<Data, Error>) -> Void)) {
        let data = self.data
        let error = self.error
        
        switch resultExpected {
        case .success:
            result(.success(data!))
        case .failure:
            result(.failure(error!))
        }
    }

}

