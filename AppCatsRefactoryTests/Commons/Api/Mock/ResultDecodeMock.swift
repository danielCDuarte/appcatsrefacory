//
//  ResultDecoderMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation

@testable import AppCatsRefactory

class ResultDecodeMock: ResultDecode {
    
    //MARK: - Properties
    var resultExpected: ResultExpectedEnum = .failure
    var data: Codable?
    var error: Error?
    
    
    //MARK: - Functions
    override func decode<T>(data: Data) -> Result<T, Error> {
        let data = self.data
        let error = self.error
        
        switch resultExpected {
        case .success:
            return .success(data as! T)
        case .failure:
            return .failure(error!)
        }
    }
    
}
