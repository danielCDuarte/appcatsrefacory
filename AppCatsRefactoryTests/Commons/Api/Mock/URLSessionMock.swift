//
//  URLSessionMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation

@testable import AppCatsRefactory

class URLSessionMock: URLSession {
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void

    //MARK: - Properties
    var data: Data?
    var error: Error?

    //MARK: - Functions
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
        let data = self.data
        let error = self.error

        return URLSessionDataTaskMock {
            completionHandler(data, nil, error)
        }
    }
}
