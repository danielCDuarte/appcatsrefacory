//
//  URLSessionDataTaskMock.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation

@testable import AppCatsRefactory

class URLSessionDataTaskMock: URLSessionDataTask {
    //MARK: - Properties
    private let closure: () -> Void

    //MARK: - Init
    init(closure: @escaping () -> Void) {
        self.closure = closure
    }

    
    //MARK: - Functions
    override func resume() {
        closure()
    }
}
