//
//  ResultExpectedEnum.swift
//  AppCatsRefactoryTests
//
//  Created by Daniel Crespo Duarte on 10/8/20.
//

import Foundation

@testable import AppCatsRefactory

enum ResultExpectedEnum {
    case success, failure
}
