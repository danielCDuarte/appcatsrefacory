//
//  BreedCatsDetailViewDelegate.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

protocol BreedCatsDetailViewDelegate: class {
    func didOpenWiki(url:URL)
}
