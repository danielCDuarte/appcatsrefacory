//
//  BreedCatsDetailContractDelegate.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Foundation

protocol BreedCatsDetailContractDelegate {
    func startLoading()
    func finishLoading()
}
