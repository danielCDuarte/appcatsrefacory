//
//  BreedCatsListViewDelegate.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

protocol BreedCatsListViewDelegate: AnyObject {
    func didSelect(breed: BreedModel)
    func didPaginator(page: Int)
}
