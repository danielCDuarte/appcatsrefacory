//
//  BreedCatsListContractDelegate.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Foundation

protocol BreedCatsListContractDelegate {
    func startLoading()
    func finishLoading()
    func setBreedCats(breedCats: [BreedModel])
    func setEmptyBreedCats(message: String)
    func setDetailBreed(detailBreed: BreedDetailModel)
    func setEmptyDetailBreed(message: String)
}
