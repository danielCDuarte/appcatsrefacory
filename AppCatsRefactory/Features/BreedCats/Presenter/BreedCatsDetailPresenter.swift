//
//  BreedCatsDetailPresenter.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation
import UIKit

protocol BreedCatsDetailPresenterProtocol {
    func openUrl(url: URL)
}

class BreedCatsDetailPresenter: BreedCatsDetailPresenterProtocol {
    
    // MARK: - Properties
    private var breedCatsDetailContractDelegate: BreedCatsDetailContractDelegate?
    
    // MARK: - Init
    func attachView(view: BreedCatsDetailContractDelegate) {
        breedCatsDetailContractDelegate = view
    }
    
    func detachView() {
        breedCatsDetailContractDelegate = nil
    }
    
    //MARK: functions
    func openUrl(url: URL) {
        self.breedCatsDetailContractDelegate?.startLoading()
        UIApplication.shared.open(url)
        self.breedCatsDetailContractDelegate?.finishLoading()
    }
    
}
