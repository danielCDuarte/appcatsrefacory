//
//  BreedCatsListPresenter.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation

protocol BreedCatsListPresenterProtocol {
    func getBreedCatsList(page: Int)
    func getDetailBreed(id: String)
}

class BreedCatsListPresenter: BreedCatsListPresenterProtocol {
    //MARK: - Properties
    private var breedCatsListContractDelegate: BreedCatsListContractDelegate?
   
    //MARK: - Init
    let apiClient: ApiClient
    init(apiClient: ApiClient) {
        self.apiClient = apiClient
    }
    
    func attachView(view: BreedCatsListContractDelegate) {
        breedCatsListContractDelegate = view
    }
    
    func detachView() {
        breedCatsListContractDelegate = nil
    }
    
    //MARK: - Functions
    
    func getBreedCatsList(page: Int) {
        self.breedCatsListContractDelegate?.startLoading()
        
        let params = ["limit": String(Constants.PAGINATOR), "page": String(page)]
        let requestParams: RequestParams = RequestParams(service: .allBreeds, method: .get, headers: nil, params: params)
        
        self.apiClient.loadRequest(requestParams: requestParams) { (result: Result<[BreedModel],Error>) in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    self.breedCatsListContractDelegate?.setBreedCats(breedCats: data)
                    self.breedCatsListContractDelegate?.finishLoading()
                }
            case .failure( _):
                DispatchQueue.main.async {
                    self.breedCatsListContractDelegate?.setEmptyBreedCats(message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""))
                    self.breedCatsListContractDelegate?.finishLoading()
                }
            }
        }
    }
    
    func getDetailBreed(id: String) {
        self.breedCatsListContractDelegate?.startLoading()
     
        let params = ["breed_ids": id]
        let requestParams: RequestParams = RequestParams(service: .detailBreed, method: .get, headers: nil, params: params)
        
        self.apiClient.loadRequest(requestParams: requestParams) { (result: Result<[BreedDetailModel],Error>) in
            switch result {
            case .success(let data):
                if let firsElement: BreedDetailModel = data.first {
                    DispatchQueue.main.async {
                        self.breedCatsListContractDelegate?.setDetailBreed(detailBreed: firsElement)
                        self.breedCatsListContractDelegate?.finishLoading()
                    }
                    return
                }
                DispatchQueue.main.async {
                    self.breedCatsListContractDelegate?.setEmptyBreedCats(message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""))
                    self.breedCatsListContractDelegate?.finishLoading()
                }
            
            case .failure( _):
                DispatchQueue.main.async {
                    self.breedCatsListContractDelegate?.setEmptyBreedCats(message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""))
                    self.breedCatsListContractDelegate?.finishLoading()
                }
            }
        }
    }
}
