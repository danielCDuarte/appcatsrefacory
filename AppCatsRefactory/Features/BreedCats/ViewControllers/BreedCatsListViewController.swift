//
//  BreedCatsListViewController.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import UIKit

class BreedCatsListViewController: UIViewController {
    
    // MARK: - Properties
    var breedList = [BreedModel]()
    
    private lazy var breedCatsListView: BreedCatsListView = {
        let view = BreedCatsListView(pagenatorIndex: 0, delegate: self)
        return view
    }()
    
    // MARK: - Init
    let breedCatsListPresenter: BreedCatsListPresenter
    
    init(breedCatsListPresenter: BreedCatsListPresenter) {
        self.breedCatsListPresenter = breedCatsListPresenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    
    // MARK: - View life cycle
    override func loadView() {
        super.loadView()
        self.view = breedCatsListView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.breedCatsListPresenter.attachView(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.breedCatsListPresenter.getBreedCatsList(page: 0)
    }
}


//MARK: BreedCatsListContractDelegate
extension BreedCatsListViewController: BreedCatsListContractDelegate {
    func startLoading() {
        self.view.showBlurLoader()
    }
    
    func finishLoading() {
        self.view.removeBluerLoader()
    }
    
    func setBreedCats(breedCats: [BreedModel]) {
        self.breedList.append(contentsOf: breedCats)
        self.breedCatsListView.setup(breedList: self.breedList)
    }
    
    func setEmptyBreedCats(message: String) {
        self.showMessageError(NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""),
            message: message,
            buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
        }
    }
    
    func setDetailBreed(detailBreed: BreedDetailModel) {
        let breedCatsDetailVC:BreedCatsDetailViewController = BreedCatsDetailViewController(breedCatsDetailPresenter: BreedCatsDetailPresenter(), breedDetail: detailBreed)
        self.navigationController?.pushViewController(breedCatsDetailVC, animated: true)
    }
    
    func setEmptyDetailBreed(message: String) {
        self.showMessageError(NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""),
            message: message,
            buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
        }
    }
    
}

//MARK: BreedCatsListViewDelegate
extension BreedCatsListViewController: BreedCatsListViewDelegate {
    func didPaginator(page: Int) {
        self.breedCatsListPresenter.getBreedCatsList(page: page)
    }
    
    func didSelect(breed: BreedModel) {
        self.breedCatsListPresenter.getDetailBreed(id: breed.id)
    }
    
}

