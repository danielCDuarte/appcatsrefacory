//
//  BreedCatsDetailViewController.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import UIKit

class BreedCatsDetailViewController: UIViewController {
    // MARK: - Properties
    private lazy var breedCatsDetailView: BreedCatsDetailView = {
        let view = BreedCatsDetailView(delegate: self)
        return view
    }()
    
    // MARK: - Init
    let breedCatsDetailPresenter: BreedCatsDetailPresenter
    let breedDetail: BreedDetailModel
    
    init(breedCatsDetailPresenter: BreedCatsDetailPresenter, breedDetail: BreedDetailModel) {
        self.breedCatsDetailPresenter = breedCatsDetailPresenter
        self.breedDetail = breedDetail
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    // MARK: - View life cycle
    override func loadView() {
        super.loadView()
        self.view = breedCatsDetailView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = breedDetail.breeds.first?.name
        self.view = breedCatsDetailView
        self.breedCatsDetailPresenter.attachView(view: self)
        breedCatsDetailView.setupView(breedDetail: self.breedDetail)
    }
    
}
// MARK: - BreedCatsDetailContractDelegate
extension BreedCatsDetailViewController: BreedCatsDetailContractDelegate {
    func startLoading() {
        self.view.showBlurLoader()
    }
    
    func finishLoading() {
        self.view.removeBluerLoader()
    }
    
}

// MARK: - BreedCatsDetailViewDelegate
extension BreedCatsDetailViewController: BreedCatsDetailViewDelegate{
    func didOpenWiki(url: URL) {
        self.breedCatsDetailPresenter.openUrl(url: url)
    }
    
}
