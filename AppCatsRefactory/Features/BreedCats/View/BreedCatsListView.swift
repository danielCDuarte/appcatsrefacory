//
//  BreedCatsListView.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import UIKit
import SnapKit
import Reusable

class BreedCatsListView: UIView {
    // MARK: - properties
    var isDataLoading:Bool = false
    
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    
    // MARK: - init
    private weak var delegate: BreedCatsListViewDelegate?
    private var pagenatorIndex: Int
    
    init(pagenatorIndex: Int, delegate: BreedCatsListViewDelegate?) {
        self.pagenatorIndex = pagenatorIndex
        self.delegate = delegate
        super.init(frame: .zero)
        setupViewConfiguration()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 50.0
        tableView.separatorStyle = .none
        tableView.register(cellType: BreedCatsCell.self)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    // MARK: - setup
    var breedList = [BreedModel]()
    func setup(breedList:[BreedModel]) {
        self.breedList = breedList
        self.isDataLoading = false
        self.tableView.reloadData()
    }
    
}

// MARK: - ViewConfiguration
extension BreedCatsListView: ViewConfiguration {
    func configureViews() {
        self.backgroundColor = Colors.Background
        self.tableView.backgroundColor = Colors.Background
    }
    func buildViewHierarchy() {
        self.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
}

// MARK: - UITableViewDelegate
extension BreedCatsListView: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.breedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let breed = self.breedList[indexPath.row]
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: BreedCatsCell.self)
        cell.configure(with: breed)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let breed: BreedModel = self.breedList[indexPath.row]
        self.delegate?.didSelect(breed: breed)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isDataLoading = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height) {
            if !isDataLoading {
                isDataLoading = true
                self.pagenatorIndex += 1
                self.delegate?.didPaginator(page: pagenatorIndex)
            }
        }
    }
    
}
