//
//  BreedCatsDetailView.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import UIKit
import SnapKit

class BreedCatsDetailView: UIView {
    // MARK: - properties
    
    private(set) lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = .white
        return view
    }()
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var photoImageView: UIImageView = {
        let photoImageVIew = UIImageView()
        photoImageVIew.contentMode = .scaleAspectFit
        photoImageVIew.image = UIImage(named: "roundBreedMedium")
        return photoImageVIew
    }()
    
    private(set) lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = Fonts.fontParagraph
        label.textColor = Colors.fontDark
        return label
    }()
    
    private(set) lazy var linkbutton: UIButton = {
        let button = UIButton()
        button.cornerRadius = 4
        button.addTarget(self, action: #selector(actionWiki(_:)), for: .touchUpInside)
        button.titleLabel?.font = Fonts.fontButton
        button.backgroundColor = Colors.lightGray
        button.setTitleColor(Colors.BackgroundMedium, for: .normal)
        return button
    }()
    
    // MARK: - init
    private weak var delegate: BreedCatsDetailViewDelegate?
    
    init(delegate: BreedCatsDetailViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        setupViewConfiguration()
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    
    // MARK: - Setup View
    private var breedDetail: BreedDetailModel?
    func setupView(breedDetail: BreedDetailModel?) {
        
        if let breedDetailValidate = breedDetail{
            self.breedDetail = breedDetailValidate
            photoImageView.downloaded(from: breedDetailValidate.url)
            self.descLabel.text = breedDetailValidate.breeds.first?.description
            
        }
    }
    
    // MARK: - Actions
    @objc func actionWiki(_ sender : UIButton){
        if let urlValidate = URL(string: breedDetail?.breeds.first?.wikipediaUrl ?? "") {
            self.delegate?.didOpenWiki(url: urlValidate)
        }
    }
}



// MARK: - View Configuration
extension BreedCatsDetailView: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.BackgroundMedium
        self.linkbutton.setTitle(NSLocalizedString("BBreedCatsDetailViewController.link", comment: ""), for: .normal)
        
    }
    
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(photoImageView)
        containerView.addSubview(descLabel)
        containerView.addSubview(linkbutton)
        
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().priority(.low)
        }
        
        photoImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(230)
        }
        
        descLabel.snp.makeConstraints { (make) in
            make.top.equalTo(photoImageView.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        linkbutton.snp.makeConstraints { (make) in
            make.height.equalTo(48)
            make.width.equalTo(200)
            make.top.equalTo(descLabel.snp.bottom).offset(16)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-16)
        }
    }
}
