//
//  BreedCatsCell.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import UIKit
import SnapKit
import Reusable

class BreedCatsCell: UITableViewCell, Reusable{
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 10
        return stackView
    }()
    
    private lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.lightGray
        return view
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.fontDark
        label.font = Fonts.fontSubTitle
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: - init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupViewConfiguration()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - View Configuration

extension BreedCatsCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.Background
    }
    
    func configure(with breed:BreedModel) {
        self.iconImageView.image = UIImage(named: "roundBreed")
        self.titleLabel.text = breed.name
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
        contentView.addSubview(bottomLine)
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(titleLabel)
        
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(bottomLine.snp.top)
        }
        
        bottomLine.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        iconImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(10)
            make.height.equalTo(20)
            make.width.equalTo(20)
        }
    }

}
