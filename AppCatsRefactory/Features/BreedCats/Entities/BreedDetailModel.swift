//
//  BreedDetailModel.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

class BreedDetailModel : Codable{
    var breeds: [BreedModel]
    var url: String
    
    enum CodingKeys: String, CodingKey {
        case breeds = "breeds"
        case url = "url"
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        breeds = try values.decode([BreedModel].self, forKey: .breeds)
        url = try values.decode(String.self, forKey: .url)
    }
    
}
