//
//  BreedModel.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

class BreedModel : Codable{
    var id: String
    var name: String
    var description: String?
    var wikipediaUrl: String?
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
        case wikipediaUrl = "wikipedia_url"
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        description = try values.decode(String.self, forKey: .description)
        wikipediaUrl = try values.decode(String.self, forKey: .wikipediaUrl)
    }
    
}
