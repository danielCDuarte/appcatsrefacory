//
//  MyCatsListViewController.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import UIKit

class MyCatsListViewController: UIViewController {
    // MARK: - Properties
    var myCatsList: [LikeCatModel] = [LikeCatModel]()
    private lazy var myCatsListView: MyCatsListView = {
        let view = MyCatsListView()
        return view
    }()
    
    // MARK: - Init
    let myCatsListPresenter: MyCatsListPresenter
    
    init(myCatsListPresenter: MyCatsListPresenter) {
        self.myCatsListPresenter = myCatsListPresenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    // MARK: - View life cycle
    override func loadView() {
        super.loadView()
        self.view = myCatsListView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myCatsListPresenter.attachView(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.myCatsListPresenter.getMyCats()
    }
    
}

// MARK: - MyCatsListContractDelegate
extension MyCatsListViewController: MyCatsListContractDelegate {
    func startLoading() {
        self.view.showBlurLoader()
    }
    
    func finishLoading() {
        self.view.removeBluerLoader()
    }
    
    func setMyCats(myCats: [LikeCatModel]) {
        self.myCatsList = myCats
        self.myCatsListView.setup(myCatsList: self.myCatsList)
    }
    
    func setEmptyMyCats(message: String) {
        self.showMessageError(NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""),
            message: message,
            buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
        }
    }
    
    
}
