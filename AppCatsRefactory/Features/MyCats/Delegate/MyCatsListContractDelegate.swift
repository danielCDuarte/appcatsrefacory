//
//  MyCatsListContractDelegate.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Foundation

protocol MyCatsListContractDelegate {
    func startLoading()
    func finishLoading()
    func setMyCats(myCats: [LikeCatModel])
    func setEmptyMyCats(message: String)
}
