//
//  MyCatsListPresenter.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation
import RealmSwift

protocol MyCatsListPresenterProtocol {
    func getMyCats()
}

class MyCatsListPresenter: MyCatsListPresenterProtocol {
    //MARK: - Properties
    private var myCatsListContractDelegate: MyCatsListContractDelegate?
    let realm: Realm
    
    //MARK: - Init
    init(realm: Realm) {
        self.realm = realm
    }
    
    func attachView(view: MyCatsListContractDelegate) {
        myCatsListContractDelegate = view
    }
    
    func detachView() {
        myCatsListContractDelegate = nil
    }
    
    // MARK: - Functions
    func getMyCats() {
        self.myCatsListContractDelegate?.startLoading()
        do {
            let list = try! Realm().objects(LikeCatModel.self)
            var listMyCats:[LikeCatModel] = [LikeCatModel]()
            for item in list {
                listMyCats.append(item)
            }
            DispatchQueue.main.async {
                self.myCatsListContractDelegate?.setMyCats(myCats: listMyCats)
                self.myCatsListContractDelegate?.finishLoading()
            }
        
        } catch _ {
            DispatchQueue.main.async {
                self.myCatsListContractDelegate?.setEmptyMyCats(message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""))
                self.myCatsListContractDelegate?.finishLoading()
            }
        }
    }
    
}
