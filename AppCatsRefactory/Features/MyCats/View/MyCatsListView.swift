//
//  MyCatsListView.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import UIKit
import SnapKit
import Reusable

class MyCatsListView: UIView {
    // MARK: - Properties
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    
    // MARK: - Init
    init() {
        super.init(frame: .zero)
        setupViewConfiguration()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 100.0
        tableView.separatorStyle = .none
        
        tableView.register(cellType: MyCatCell.self)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    // MARK: - Setup
    var myCatsList: [LikeCatModel] = [LikeCatModel]()
    func setup(myCatsList:[LikeCatModel]){
        self.myCatsList = myCatsList
        self.tableView.reloadData()
    }
    
}

// MARK: - ViewConfiguration
extension MyCatsListView: ViewConfiguration {
    func configureViews() {
        self.backgroundColor = Colors.Background
        self.tableView.backgroundColor = Colors.Background
    }
    
    func buildViewHierarchy() {
        self.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: - UITableViewDelegate
extension MyCatsListView: UITableViewDelegate, UITableViewDataSource{ 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.myCatsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCat = self.myCatsList[indexPath.row]
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: MyCatCell.self)
        cell.configure(with: myCat)
        return cell
    }
}
