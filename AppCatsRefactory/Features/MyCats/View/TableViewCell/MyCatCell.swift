//
//  MyCatCell.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import UIKit
import SnapKit
import Reusable

class MyCatCell: UITableViewCell, Reusable{
    
    // MARK: - Properties
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 10
        return stackView
    }()
    
    private lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.lightGray
        return view
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var stackViewItems: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.spacing = 3
        return stackView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.lightGray
        label.font = Fonts.fontSubTitle
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.lightGray
        label.font = Fonts.fontSubTitle
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var stackViewLike: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 5
        return stackView
    }()
    
    private lazy var likeLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.DarkGray
        label.font = Fonts.fontButton
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var iconlikeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupViewConfiguration()
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - ViewConfiguration
extension MyCatCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.Background
        iconImageView.layer.cornerRadius = 40
        iconImageView.layer.borderWidth = 0.5
        iconImageView.layer.borderColor = Colors.lightGray.cgColor
        iconImageView.clipsToBounds = true
    }
    
    func configure(with likeCat:LikeCatModel) {
        self.iconImageView.image = UIImage(named: "roundBreed")
        self.nameLabel.text = "\(NSLocalizedString("MyCatsDetailViewController.name", comment: ""))\(likeCat.name)"
        self.dateLabel.text = "\(NSLocalizedString("MyCatsDetailViewController.dateCreated", comment: ""))\(likeCat.createdAt)"
        self.likeLabel.text = NSLocalizedString("MyCatsDetailViewController.like", comment: "")
        self.iconlikeImageView.image = UIImage(named: likeCat.isLike ? "happy" : "close")
        self.iconImageView.image = UIImage(named: "roundBreedMedium")
        self.iconImageView.downloaded(from: likeCat.url)
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
        contentView.addSubview(bottomLine)
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(stackViewItems)
        stackViewItems.addArrangedSubview(nameLabel)
        stackViewItems.addArrangedSubview(dateLabel)
        
        stackViewItems.addArrangedSubview(stackViewLike)
        stackViewLike.addArrangedSubview(likeLabel)
        stackViewLike.addArrangedSubview(iconlikeImageView)
        
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(bottomLine.snp.top)
        }
        
        bottomLine.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        iconImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(10)
            make.height.equalTo(80)
            make.width.equalTo(80)
        }
        stackViewLike.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
        }
        iconlikeImageView.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.width.equalTo(30)
        }
        
    }

}
