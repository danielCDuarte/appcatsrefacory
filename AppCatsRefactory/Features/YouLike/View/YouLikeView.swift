//
//  YouLikeView.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import UIKit
import SnapKit

class YouLikeView: UIView {
    // MARK: - Properties
    
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private(set) lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.fontDark
        label.font = Fonts.fontSubTitle
        label.textAlignment = .center
        return label
    }()
    
    private lazy var photoImageView: UIImageView = {
        let photoImageVIew = UIImageView()
        photoImageVIew.contentMode = .scaleAspectFit
        photoImageVIew.image = UIImage(named: "roundBreedMedium")
        return photoImageVIew
    }()
    
    private lazy var footerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 10
        return stackView
    }()
    
    private(set) lazy var likeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(actionLike(_:)), for: .touchUpInside)
        button.setImage(UIImage(named: "happy"), for: .normal)
        return button
    }()
    
    private(set) lazy var disLikeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(actionDisLike(_:)), for: .touchUpInside)
        button.setImage(UIImage(named: "close"), for: .normal)
        return button
    }()
    
    // MARK: - Init
    private weak var delegate: YouLikeDelegate?
    
    init(delegate: YouLikeDelegate?) {
        self.delegate = delegate
        
        super.init(frame: .zero)
        setupViewConfiguration()
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    
    // MARK: - Setup
    var youLikeModel: YouLikeModel?
    func setup(youLikeModel: YouLikeModel) {
        self.youLikeModel = youLikeModel
        self.nameLabel.text = youLikeModel.imageId
        self.photoImageView.image = UIImage(named: "roundBreedMedium")
        if let urlString: String = youLikeModel.image?.url {
            self.photoImageView.downloaded(from: urlString)
        }
        
    }
    
    // MARK: - Actions
    @objc func actionLike(_ sender : UIButton) {
        if let youLikeModelValidate = youLikeModel {
            youLikeModelValidate.isLike = true
            self.delegate?.didItsLike(youLike: youLikeModelValidate)
        }
        
    }
    
    @objc func actionDisLike(_ sender : UIButton) {
        if let youLikeModelValidate = youLikeModel {
            youLikeModelValidate.isLike = false
            self.delegate?.didItsLike(youLike: youLikeModelValidate)
        }
        
    }
    
}


// MARK: - ViewConfiguration
extension YouLikeView: ViewConfiguration {
    
    func configureViews() {
        self.backgroundColor = Colors.BackgroundMedium
    }
    
    func buildViewHierarchy() {
        addSubview(containerView)
        containerView.addSubview(nameLabel)
        containerView.addSubview(photoImageView)
        containerView.addSubview(footerStackView)
        footerStackView.addArrangedSubview(disLikeButton)
        footerStackView.addArrangedSubview(likeButton)
    }
    
    func setupConstraints() {
       
        containerView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            if #available(iOS 11, *) {
                make.top.equalTo(safeAreaLayoutGuide.snp.topMargin)
            } else {
                make.top.equalTo(self)
            }
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(100)
        }
        
        photoImageView.snp.makeConstraints{ (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        footerStackView.snp.makeConstraints { (make) in
            make.top.equalTo(photoImageView.snp.bottom).offset(16)
            make.height.equalTo(150)
            make.leading.trailing.equalToSuperview()
            if #available(iOS 11, *) {
                make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin).offset(-16)
            } else {
                make.bottom.equalTo(self).offset(-16)
            }
        }
        
    }
}
