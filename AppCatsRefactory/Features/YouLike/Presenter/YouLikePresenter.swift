//
//  YouLikePresenter.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import Foundation
import RealmSwift

protocol YouLikePresenterProtocol {
    func getFavorites(page: Int)
    func saveFavorite(youLike: YouLikeModel)
}

class YouLikePresenter: YouLikePresenterProtocol {
    // MARK: - Init
    let apiClient: ApiClient
    let realm: Realm
    private var youLikeContractDelegate: YouLikeContractDelegate?
    
    init(apiClient: ApiClient, realm: Realm) {
        self.apiClient = apiClient
        self.realm = realm
    }
    
    func attachView(view: YouLikeContractDelegate) {
        youLikeContractDelegate = view
    }
    
    func detachView() {
        youLikeContractDelegate = nil
    }
    
    // MARK: - Functions
    
    func getFavorites(page: Int) {
        self.youLikeContractDelegate?.startLoading()
        
        let params = ["limit": String(Constants.PAGINATOR), "page": String(page), "order": "DESC"]
        let requestParams: RequestParams = RequestParams(service: .favorites, method: .get, headers: nil, params: params)
        
        self.apiClient.loadRequest(requestParams: requestParams) { (result: Result<[YouLikeModel],Error>) in
            switch result {
            case .success(let data):
                if data.count >= 1 {
                    DispatchQueue.main.async {
                        self.youLikeContractDelegate?.setFavorites(YouLikes: data)
                        self.youLikeContractDelegate?.finishLoading()
                    }
                    return
                }
                DispatchQueue.main.async {
                    self.youLikeContractDelegate?.setEmptyFavorites(title: NSLocalizedString("YouLikeViewController.errorTitle", comment: ""),
                        message: NSLocalizedString("YouLikeViewController.errorDesc", comment: "")
                    )
                    self.youLikeContractDelegate?.finishLoading()
                }
                
            case .failure( _):
                DispatchQueue.main.async {
                    self.youLikeContractDelegate?.setEmptyFavorites(title: NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""),
                        message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: "")
                    )
                    self.youLikeContractDelegate?.finishLoading()
                }
                
            }
            
        }
    }
    
    func saveFavorite(youLike: YouLikeModel) {
        self.youLikeContractDelegate?.startLoading()
        
        let newLikeCat = LikeCatModel()
        newLikeCat.name = youLike.imageId
        newLikeCat.createdAt = youLike.createdAt ?? ""
        newLikeCat.isLike = youLike.isLike
        newLikeCat.url = youLike.image?.url ?? ""
        
        do {
            try realm.write {
                realm.add(newLikeCat)
                self.youLikeContractDelegate?.setSaveFavorite()
                self.youLikeContractDelegate?.finishLoading()
            }
        } catch {
            self.youLikeContractDelegate?.setErrorSaveFavorite(message: NSLocalizedString("BreedCatsListViewController.errorDesc", comment: ""))
            self.youLikeContractDelegate?.finishLoading()
        }
    }
    
}
