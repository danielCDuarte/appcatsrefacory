//
//  LikeCatModel.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation
import RealmSwift

class LikeCatModel: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var createdAt: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var isLike: Bool = false
}
