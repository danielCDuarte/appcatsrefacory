//
//  YouLikeModel.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

class YouLikeModel : Codable{
    var id: Int
    var createdAt: String?
    var imageId: String
    var image: YouLikeImageModel?
    var isLike: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case createdAt = "created_at"
        case imageId = "image_id"
        case image = "image"
        
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
        createdAt = try values.decode(String.self, forKey: .createdAt)
        imageId = try values.decode(String.self, forKey: .imageId)
        image = try values.decode(YouLikeImageModel.self, forKey: .image)
    }
    
}


class YouLikeImageModel : Codable{
    var url: String
    enum CodingKeys: String, CodingKey {
        case url = "url"
        
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decode(String.self, forKey: .url)
    }
}
