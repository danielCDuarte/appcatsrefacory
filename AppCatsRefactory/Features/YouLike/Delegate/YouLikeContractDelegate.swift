//
//  YouLikeContractDelegate.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import Foundation

protocol YouLikeContractDelegate {
    func startLoading()
    func finishLoading()
    func setFavorites(YouLikes: [YouLikeModel])
    func setEmptyFavorites(title: String, message: String)
    func setSaveFavorite()
    func setErrorSaveFavorite(message: String)
}
