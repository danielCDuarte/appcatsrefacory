//
//  YouLikeDelegate.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

protocol YouLikeDelegate: class {
    func didItsLike(youLike:YouLikeModel)
}
