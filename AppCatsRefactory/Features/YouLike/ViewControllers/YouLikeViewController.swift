//
//  YouLikeViewController.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import UIKit

class YouLikeViewController: UIViewController {
    // MARK: - Properties
    private lazy var youLikeView: YouLikeView = {
        let view = YouLikeView(delegate: self)
        return view
    }()
    
    // MARK: - Init
    private var page: Int = 0
    private var index: Int = 0
    let userDetafult = UserDefaults.standard
    var youLikeList = [YouLikeModel]()
    
    let youLikePresenter: YouLikePresenter
    
    init(youLikePresenter: YouLikePresenter) {
        self.youLikePresenter = youLikePresenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    // MARK: - View life cycle
    override func loadView() {
        super.loadView()
        self.view = youLikeView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.youLikePresenter.attachView(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.page = userDetafult.integer(forKey: "page")
        self.index = userDetafult.integer(forKey: "index")
        self.youLikePresenter.getFavorites(page: self.page)
    }
    
    //MARK: - Functions
    func validatePaginator() {
        userDetafult.set(self.index, forKey: "index")
        if index + 1 >= self.youLikeList.count{
            self.page += 1
            self.index = 0
            userDetafult.set(self.index, forKey: "page")
            userDetafult.set(self.page, forKey: "page")
            self.youLikePresenter.getFavorites(page: self.page)
            return
        }
        
        self.youLikeView.setup(youLikeModel: self.youLikeList[index])
    }
    
}

// MARK: - YouLikeContractDelegate
extension YouLikeViewController: YouLikeContractDelegate {
 
    func startLoading() {
        self.view.showBlurLoader()
    }
    
    func finishLoading() {
        self.view.removeBluerLoader()
    }
    
    func setFavorites(YouLikes: [YouLikeModel]) {
        self.youLikeList = YouLikes
        self.youLikeView.setup(youLikeModel: self.youLikeList[self.index])
    }
    func setEmptyFavorites(title: String, message: String) {
        self.showMessageError(title,
            message: message,
            buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
        }
    }
    
    func setSaveFavorite() {
        self.index += 1
        self.validatePaginator()
    }
    
    func setErrorSaveFavorite(message: String) {
        self.showMessageError(NSLocalizedString("BreedCatsListViewController.errorTitle", comment: ""),
            message: message,
            buttonTitle: NSLocalizedString("BreedCatsListViewController.btn", comment: "")) {
        }
    }
}

// MARK: - YouLikeDelegate
extension YouLikeViewController: YouLikeDelegate {
    func didItsLike(youLike: YouLikeModel) {
        self.youLikePresenter.saveFavorite(youLike: youLike)
    }
    
}
