//
//  EnumApiClient.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//
import Foundation

enum HttpMethods: String {
    case post = "POST"
    case put = "PUT"
    case get = "GET"
    case delete = "DELETE"
}

enum APIClientError: Error {
    case noData
    case urlNotValid
}

struct Resource {
    let url: URL
    let method: HttpMethods
}
