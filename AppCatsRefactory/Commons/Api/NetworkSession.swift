//
//  NetworkSession.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/7/20.
//

import Foundation

protocol NetworkSessionProtocol {
    func didLoad (_ request: URLRequest, result: @escaping ((Result<Data, Error>) -> Void))
}

class NetworkSession : NetworkSessionProtocol {
    
    //MARK: properties
    let  session: URLSession

    init(session: URLSession) {
        self.session = session
    }
    
    func didLoad (_ request: URLRequest, result: @escaping ((Result<Data, Error>) -> Void)) {
        let task = session.dataTask(with: request, completionHandler: { (data, _, error) in
            guard let dataValidate = data else {
                result(.failure(APIClientError.noData))
                return
            }
            if let errorValidate = error {
                result(.failure(errorValidate))
            }
            result(.success(dataValidate))
        })
        task.resume()
    }
    
}

