//
//  ApiClient.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/6/20.
//

import Foundation

typealias RequestParams = (service: enumServices, method: HttpMethods, headers: [String : String]?, params: [String : String]?)

protocol ApiClientProtocol {
    func loadRequest <T:Codable>(requestParams: RequestParams, completionHandler: @escaping ((Result<T, Error>) -> Void))
}

class ApiClient: ApiClientProtocol {
    
    let networkSession: NetworkSession
    let resultDecoder: ResultDecode
    
    init(networkSession: NetworkSession, resultDecoder: ResultDecode) {
        self.networkSession = networkSession
        self.resultDecoder = resultDecoder
    }
    
    func loadRequest<T:Codable>(requestParams: RequestParams, completionHandler: @escaping ((Result<T, Error>) -> Void)) {
        var urlString:String = "\(Constants.BASEURL)\(requestParams.service.rawValue)"
       
        if (requestParams.method == .get) { urlString.append(requestParams.params?.queryString ?? "") }
        
        guard let urlValidate:URL = URL(string: urlString) else {
            completionHandler(.failure(APIClientError.urlNotValid))
            return
        }
        
        let resource = Resource(url: urlValidate, method: requestParams.method)
        var request = URLRequest(resource)
        
        request.addHeadersParams(Constants.headersBase, paramsAdditional: requestParams.headers)
        
        if (requestParams.method == .post) { request.httpBody = requestParams.params?.convertForData }
       
        networkSession.didLoad(request) { (result: Result<Data,Error>) in
            switch result {
            case .success(let data):
                let resultDecode: Result<T, Error> = self.resultDecoder.decode(data: data)
                switch resultDecode {
                case .success(let model):
                    completionHandler(.success(model))
                case .failure(let error):
                    completionHandler(.failure(error))
                }
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    
}





