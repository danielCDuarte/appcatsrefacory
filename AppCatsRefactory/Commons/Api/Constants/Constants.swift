//
//  Constants.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

struct Constants{
    //base url
    static var BASEURL: String {
        guard let baseUrl = Bundle.main.infoDictionary?["API_BASE_URL_ENDPOINT"] as? String else {
            return ""
        }
        return baseUrl
    }
    static var APIKEY: String {
        guard let apiKey = Bundle.main.infoDictionary?["API_KEY"] as? String else {
            return ""
        }
        return apiKey
    }
    
    static let headersBase: [String : String] = [
        "Content-Type": "application/json",
        "Accept": "application/json",
        "x-api-key": Self.APIKEY
    ]
    
    static let PAGINATOR: Int = 20
   
}

enum enumServices: String {
    case allBreeds = "breeds"
    case detailBreed = "images/search"
    case favorites = "favourites"
}

