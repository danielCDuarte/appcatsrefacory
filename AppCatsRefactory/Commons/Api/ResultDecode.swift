//
//  ResultDecode.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/5/20.
//

import Foundation

protocol ResultDecodeProtocol {
    func decode<T: Codable>(data: Data) -> Result<T, Error>
}

class ResultDecode: ResultDecodeProtocol {
    func decode<T: Codable>(data: Data) -> Result<T, Error> {
        do {
            let decoder = JSONDecoder()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            decoder.dateDecodingStrategy = .formatted(formatter)
            let decoded = try decoder.decode(T.self, from: data)
            return .success(decoded)
        } catch {
            return .failure(error)
        }
    }
}
