//
//  Colors.swift
//  AppCatsRefactory
//
//  Created by condor on 10/1/20.
//

import UIKit

struct Colors {
    static let DarkGray:UIColor = #colorLiteral(red: 0.2980392157, green: 0.3843137255, blue: 0.431372549, alpha: 1)
    static let lightGray:UIColor = #colorLiteral(red: 0.568627451, green: 0.6392156863, blue: 0.6823529412, alpha: 1)
    static let Background:UIColor = #colorLiteral(red: 0.9606963992, green: 0.9607838988, blue: 0.964971602, alpha: 1)
    static let BackgroundMedium:UIColor = #colorLiteral(red: 0.88143152, green: 0.8865520358, blue: 0.8819229007, alpha: 1)
    static let fontDark:UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let fontLight:UIColor = #colorLiteral(red: 0.7136489749, green: 0.7137710452, blue: 0.713631928, alpha: 1)
    static let blackOpacity:UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3962643046)
}
