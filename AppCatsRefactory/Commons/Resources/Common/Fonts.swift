//
//  Fonts.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/1/20.
//

import Foundation
import UIKit

struct Fonts {
    static let fontNavTitle:UIFont = UIFont(name: "AppleSDGothicNeo-Regular", size: 16)!
    static let fontParagraph:UIFont = UIFont(name: "AppleSDGothicNeo-Thin", size: 14)!
    static let fontSubTitle:UIFont = UIFont(name: "AppleSDGothicNeo-UltraLight", size: 14)!
    static let fontButton:UIFont = UIFont(name: "AppleSDGothicNeo-Medium", size: 14)!
}
