//
//  UIViewController+Extension.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/13/20.
//

import UIKit

extension UIViewController {
    
    func showMessageError(_ title: String, message: String, buttonTitle:String,_ completion: @escaping (() -> Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: {(alert: UIAlertAction!) in
            completion()
        }))
        self.present(alert, animated: true)
        
    }
    
    func addTabBarIcon(imageNamed: String) {
        let tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: imageNamed),
            selectedImage: UIImage(named: imageNamed)
        )
        tabBarItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        self.tabBarItem = tabBarItem
    }
    
}

