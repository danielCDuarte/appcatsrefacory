//
//  UITabBar+Extension.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import UIKit

extension UITabBar {
    
    func configure(){
        self.barTintColor = Colors.BackgroundMedium
        self.unselectedItemTintColor = Colors.lightGray
        self.tintColor = Colors.DarkGray
    }
    
}
