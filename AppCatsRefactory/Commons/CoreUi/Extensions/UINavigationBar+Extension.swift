//
//  UINavigationBar+Extension.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/9/20.
//

import UIKit

extension UINavigationBar {
    
    func configure() {
        UINavigationBar.appearance().tintColor = Colors.lightGray
        let attrs:[NSAttributedString.Key : Any] = [NSAttributedString.Key.font: Fonts.fontNavTitle]
        UINavigationBar.appearance().titleTextAttributes = attrs
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: UIControl.State.highlighted)
    }
    
}
