//
//  URLRequestExtension.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/7/20.
//

import Foundation

extension URLRequest {
    
    init(_ resource: Resource) {
        self.init(url: resource.url)
        self.httpMethod = resource.method.rawValue
    }
    
    mutating func addHeadersParams(_ paramsBase: [String : String], paramsAdditional: [String : String]?) {
        for (key, value) in paramsBase {
            self.setValue(value, forHTTPHeaderField: key)
        }
        guard let validateParamsAdditional = paramsAdditional else { return }
        for (key, value) in validateParamsAdditional {
            self.setValue(value, forHTTPHeaderField: key)
        }
    }
    
}
