//
//  DictionaryExtensions.swift
//  AppCatsRefactory
//
//  Created by Daniel Crespo Duarte on 10/7/20.
//

import Foundation

extension Dictionary {
    var queryString: String {
        if(self.isEmpty){
            return ""
        }
        let stringKeyValue: String = self.compactMap({ (key, value) -> String in
            return "\(key)=\(value)"
        }).joined(separator: "&")

        return "?\(stringKeyValue)"
        
    }
    
    var convertForData: Data? {
        if(self.isEmpty){
            return nil
        }
        return try! JSONSerialization.data(withJSONObject: self, options: [])
    }
}
