//
//  AppDelegate.swift
//  AppCatsRefactory
//
//  Created by condor on 10/1/20.
//

import UIKit
import RealmSwift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // send that into our coordinator so that it can display view controllers
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        
        // create a basic UIWindow and activate it
        window.makeKeyAndVisible()
        
        UINavigationBar.appearance().configure()
        
        let apiClient: ApiClient = ApiClient(networkSession: NetworkSession(session: URLSession(configuration: .default)), resultDecoder: ResultDecode())
        let realm: Realm = try! Realm()
        
        let tabBarController:TabBarAppController = TabBarAppController()
        
        // Create Tab BreedCats
        let navigationBreedCatsController = UINavigationController()
        let tabBreedCatsViewController = BreedCatsListViewController(breedCatsListPresenter: BreedCatsListPresenter(apiClient: apiClient))
        tabBreedCatsViewController.title =  NSLocalizedString("BreedCatsListViewController.BreedCats", comment: "")
        tabBreedCatsViewController.addTabBarIcon(imageNamed: "footPrintTab")
        navigationBreedCatsController.pushViewController(tabBreedCatsViewController, animated: false)
        
        //Create Tab YouLike
        let navigationYouLikeController = UINavigationController()
        let youLikeViewController: YouLikeViewController = YouLikeViewController(youLikePresenter: YouLikePresenter(apiClient: apiClient, realm: realm))
        youLikeViewController.title =  NSLocalizedString("BreedCatsListViewController.YouLike", comment: "")
        youLikeViewController.addTabBarIcon(imageNamed: "heartHalfTab")
        navigationYouLikeController.pushViewController(youLikeViewController, animated: false)
        
        // Create Tab MyCats
        let navigationMyCatsListController = UINavigationController()
        let myCatsListViewController = MyCatsListViewController(myCatsListPresenter: MyCatsListPresenter(realm: realm))
        myCatsListViewController.title = NSLocalizedString("BreedCatsListViewController.MyCats", comment: "")
        myCatsListViewController.addTabBarIcon(imageNamed: "catOnlineTab")
        navigationMyCatsListController.pushViewController(myCatsListViewController, animated: false)
        
        tabBarController.viewControllers = [navigationBreedCatsController, navigationYouLikeController, navigationMyCatsListController]
        self.window?.rootViewController = tabBarController
        
        return true
    }
    
    
    
}

